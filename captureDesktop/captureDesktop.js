const path = require('path');
const url = require('url');
const { BrowserWindow } = require('electron');

let captureDesktopWindow;

const ipcEvents = {
  CAPTURE_DESKTOP: 'capture-desktop',
};

const captureDesktop = () => {
  captureDesktopWindow = new BrowserWindow({
    width: 1,
    height: 1,
    frame: false,
    transparent: true,
  });

  captureDesktopWindow.loadFile(url.format({
    pathname: path.join(__dirname, 'captureDesktopWindow.html'),
  }));

  captureDesktopWindow.on('show', () => {
    setTimeout(() => {
      captureDesktopWindow.webContents.send(ipcEvents.CAPTURE_DESKTOP);
      captureDesktopWindow.hide();
    }, 1000);
  });

  captureDesktopWindow.on('closed', () => {
    captureDesktopWindow = null;
  });
};

exports = module.exports = captureDesktop;
exports.ipcEvents = module.exports.ipcEvents = ipcEvents;
