const { ipcRenderer, desktopCapturer } = require('electron');
const screen = require('electron').screen;

const { ipcEvents } = require('./captureDesktop');

const determineScreenShotSize = () => {
  const screenSize = screen.getPrimaryDisplay().workAreaSize;
  const maxDimension = Math.max(screenSize.width, screenSize.height);

  return {
    width: maxDimension * window.devicePixelRatio,
    height: maxDimension * window.devicePixelRatio,
  };
};

ipcRenderer.on(ipcEvents.CAPTURE_DESKTOP, () => {
  const thumbSize = determineScreenShotSize();

  const options = {
    types: ['screen', 'window'],
    thumbnailSize: thumbSize,
  };

  desktopCapturer.getSources(options, (err, sources) => {
    if (err) {
      throw (err);
    }

    sources.forEach(source => {
      if (source.name === 'Entire screen' || source.name === 'Screen 1') {
        ipcRenderer.send(ipcEvents.CAPTURE_DESKTOP, {
          encodedImg: source.thumbnail.toDataURL(),
        });
      }
    });
  });
});
