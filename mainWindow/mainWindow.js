const path = require('path');
const url = require('url');
const { ipcMain, BrowserWindow } = require('electron');

const ipcEvents = require('../ipcEvents');
const captureDesktop = require('../captureDesktop/captureDesktop');
const captureDesktopIpcEvents = require('../captureDesktop/captureDesktop').ipcEvents;
const getWindowPosition = require('../utils/getWindowPosition');

let mainWindow;

const createMainWindow = tray => {
  mainWindow = new BrowserWindow({
    width: 400,
    height: 200,
    fullscreenable: false,
    resizable: false,
    frame: false,
    webPreferences: {
      // Prevents renderer process code from not running when window is hidden
      backgroundThrottling: false,
    },
  });

  const { x, y } = getWindowPosition(mainWindow, tray);
  mainWindow.setPosition(x, y);

  mainWindow.loadFile(url.format({
    pathname: path.join(__dirname, 'mainWindow.html'),
  }));

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
};

const toggleWindow = () => {
  if (mainWindow.isVisible()) {
    mainWindow.hide();
  } else {
    mainWindow.show();
  }
};

ipcMain.on(captureDesktopIpcEvents.CAPTURE_DESKTOP, (event, args) => {
  mainWindow.show();
  if (mainWindow !== null) {
    mainWindow.webContents.send(ipcEvents.SCREENSHOT_OBTAINED, args);
  }
});

ipcMain.on(ipcEvents.REQUEST_SCREENSHOT, () => {
  captureDesktop();
  mainWindow.hide();
});

exports.createMainWindow = createMainWindow;
exports.toggleWindow = toggleWindow;
