/* eslint-disable object-shorthand, no-new */
const { remote } = require('electron');
const Vue = require('vue/dist/vue.common');
const VueElectron = require('vue-electron');
const nozbe = require('node-nozbe');
const cloudinary = require('cloudinary');

const { CLOUDINARY_CONFIG } = require('../config');
const ipcEvents = require('../ipcEvents');
const getBase64Dimensions = require('../utils/getBase64Dimensions');
const calculateWindowHeight = require('../utils/calculateWindowHeight');

const accessToken = remote.getGlobal('accessToken');
const clientId = remote.getGlobal('clientId');

cloudinary.config(CLOUDINARY_CONFIG);

Vue.use(VueElectron);

new Vue({
  el: '#mainWindow',
  data: {
    taskName: '',
    screenshotData: null,
    isAddingScreenshot: false,
  },
  mounted: function() {
    this.$electron.ipcRenderer.on(ipcEvents.SCREENSHOT_OBTAINED, (event, args) => {
      this.screenshotData = args.encodedImg;
    });
  },
  methods: {
    makeScreenshot: function() {
      this.$electron.ipcRenderer.send(ipcEvents.REQUEST_SCREENSHOT);
    },
    addTask: function() {
      if (this.screenshotData && accessToken && clientId) {
        this.isAddingScreenshot = true;
        cloudinary.v2.uploader.upload(this.screenshotData, (err, res) => {
          if (err) {
            console.error(err);
          }

          const imageUrl = res.secure_url;

          const task = {
            name: this.taskName,
          };

          nozbe.addTask(clientId, accessToken, task)
            .then(res => {
              const comment = {
                taskId: res.id,
                body: imageUrl,
              };

              nozbe.addComment(clientId, accessToken, comment)
                .then(() => {
                  this.screenshotData = null;
                  this.taskName = '';

                  this.$electron.remote.getCurrentWindow().hide();

                  new Notification('Success', {
                    body: 'Screenshot added to Inbox',
                  });
                  this.isAddingScreenshot = false;
                });
            });
        });
      }
    },
    cancel: function() {
      this.screenshotData = null;
      this.$electron.remote.getCurrentWindow().hide();
    },
  },
  watch: {
    screenshotData: async function(newScreenshotData) {
      const FOOTER_SIZE = 36;

      const windowWidth = this.$electron.remote.getCurrentWindow().getSize()[0];
      const { imageWidth, imageHeight } = await getBase64Dimensions(newScreenshotData);
      const newWindowHeight = calculateWindowHeight(windowWidth, imageWidth, imageHeight) + FOOTER_SIZE;

      this.$electron.remote.getCurrentWindow().setSize(windowWidth, newWindowHeight);
    },
  },
});
