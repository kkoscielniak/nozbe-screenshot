const { app, Tray, globalShortcut } = require('electron');
const settings = require('electron-settings');

const config = require('./config');
const { createLoginWindow } = require('./loginWindow/loginWindow');
const { createMainWindow, toggleWindow } = require('./mainWindow/mainWindow');
const getTrayIconPath = require('./utils/getTrayIconPath');
const captureDesktop = require('./captureDesktop/captureDesktop');

let tray;
let accessToken;

app.dock.hide();

const createTray = callback => {
  const iconPath = getTrayIconPath();

  tray = new Tray(iconPath);
  tray.on('right-click', toggleWindow);
  tray.on('double-click', toggleWindow);
  tray.on('click', () => {
    toggleWindow();
  });

  if (callback) {
    callback();
  }
};

const startApp = () => {
  accessToken = settings.get('accessToken');

  if (!accessToken) {
    createLoginWindow();
  } else {
    global.accessToken = accessToken;
    global.clientId = config.CLIENT_ID;

    createTray(() => {
      createMainWindow(tray);
    });

    globalShortcut.register('CommandOrControl+Alt+T', () => {
      captureDesktop();
    });
  }
};

app.on('ready', startApp);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

settings.watch('accessToken', newVal => {
  accessToken = newVal;
});
