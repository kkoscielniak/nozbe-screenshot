const ipcEvents = {
  REQUEST_SCREENSHOT: 'request-screenshot',
  SCREENSHOT_OBTAINED: 'screenshot-obtained',
};

exports = module.exports = ipcEvents;
