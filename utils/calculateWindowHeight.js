const calculateWindowHeight = (windowWidth, imageWidth, imageHeight) => (imageHeight * windowWidth) / imageWidth;
exports = module.exports = calculateWindowHeight;
