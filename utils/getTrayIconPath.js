const path = require('path');
const platform = require('os').platform();
const { app } = require('electron');

const assetsDirectory = path.join(app.getAppPath(), 'assets');

const getTrayIconPath = () => {
  switch (platform) { // eslint-disable-line default-case
    case 'darwin':
      return path.join(assetsDirectory, 'icon.png');
    case 'win32':
      return path.join(assetsDirectory, 'icon.ico');
  }
};

exports = module.exports = getTrayIconPath;
