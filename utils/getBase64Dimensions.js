const getImageDimensions = base64Data => new Promise(resolved => {
  const image = new Image();
  image.onload = () => {
    resolved({ imageWidth: image.width, imageHeight: image.height });
  };
  image.src = base64Data;
});
exports = module.exports = getImageDimensions;
