const { BrowserWindow } = require('electron');
const settings = require('electron-settings');
const nozbe = require('node-nozbe');

const config = require('../config');

let loginWindow;

const getAccessTokenFromUrl = urlString => {
  const url = new URL(urlString);
  return url.searchParams.get('access_token');
};

const createLoginWindow = () => {
  const nozbeOAuthUrl = (nozbe.getOAuthLoginURL(config.CLIENT_ID));

  loginWindow = new BrowserWindow({
    width: 800,
    height: 600,
    'node-integration': false,
    'web-security': false,
  });
  loginWindow.loadURL(nozbeOAuthUrl);

  loginWindow.on('close', () => {
    loginWindow = null;
  });

  loginWindow.webContents.on('did-navigate', (event, url) => {
    const accessToken = getAccessTokenFromUrl(url);

    if (accessToken) {
      settings.set('accessToken', accessToken);

      loginWindow.hide();
    }
  });
};

exports.createLoginWindow = module.exports.createLoginWindow = createLoginWindow;
